import { HttpFunction } from '@google-cloud/functions-framework/build/src/functions';



// bring in Feathers API
import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';

// bring in application specific elements
import { Application } from './declarations';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import { HookContext as FeathersHookContext } from '@feathersjs/feathers';
import mongoose from './mongoose';

// construct Feathers app
const app = feathers();
export type HookContext<T = any> = { app: Application } & FeathersHookContext<T>;

// Load app configuration
app.configure(configuration());

// Load mongoose configuration
app.configure(mongoose);


// Configure other middleware (see `middleware/index.ts`)
app.configure(middleware);
// Set up our services (see `services/index.ts`)
app.configure(services);
// Set up event channels (see channels.ts)
app.configure(channels);

// register applicaton level hooks
app.hooks(appHooks);


export const feathersGCF: HttpFunction = async (req, res) => {
  console.log( "method: " + req.method );
  console.log( "path: " + req.path );
  res.send('Unsupported request');
};
