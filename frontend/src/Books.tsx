import React, { useState, useEffect, FormEvent } from 'react';
import { Paginated } from '@feathersjs/feathers';
import client from './feathers';

interface Book {
  // add the required properties here
  isbn: number,
  title: string,
  pages: number,
  _id: string
};

type BookFunction = (g: Book) => void;

interface AllBookFuntions {
  addBook: BookFunction,
  removeBook: BookFunction
};

let bookFuncs: AllBookFuntions = {
  addBook: (c: Book) => {},
  removeBook: (c: Book) => {}
};

const booksService = client.service('books');

booksService.on( 'created', (newBook: Book) => {
  bookFuncs.addBook( newBook );
});

booksService.on( 'removed', (oldBook: Book) => {
  bookFuncs.removeBook( oldBook );
});

function Books() {
  const [isbn, setIsbn] = useState(1);
  const [title, setTitle] = useState("");
  const [pages, setPages] = useState(100);
  const [allBooks, setAllBooks] = useState<Array<Book>>([]);


  const handleDelete = (id: string) => {
      booksService.remove( id );
  }

  const bookRows = allBooks.map( (book: Book ) =>
    <tr key={book._id}>
      <td>{book._id}</td>
      <td>{book.isbn}</td>
      <td>{book.title}</td>
      <td>{book.pages}</td>
      <td><button onClick={() => handleDelete(book._id)} type="button" className="btn btn-danger">Delete</button></td>
    </tr>
  );

  useEffect(() => {
    function addBookX( newBook: Book ) {
      setAllBooks( [...allBooks, newBook] );
    }

    function removeBookX( oldBook: Book ) {
      const newBooks = allBooks.filter((ibook,index,arr) => {
        return ibook._id !== oldBook._id;
      });
      setAllBooks( newBooks );
    }

    bookFuncs.addBook = addBookX;
    bookFuncs.removeBook = removeBookX;
  });

  useEffect(() => {
    booksService
    .find()
    .then( (bookPage: Paginated<Book>) => setAllBooks( bookPage.data ))
    .catch( (err: any) => {
      console.log( "problem finding books.");
    });
  }, []);

  const handleSubmit = (e: FormEvent) => {
      e.preventDefault();
      const element = e.currentTarget as HTMLFormElement;
      if ( element.checkValidity() ) {
        element.classList.remove('was-validated');
        booksService
        .create({isbn, title, pages})
        .then( (book: Book) => {
          setIsbn(1);
          setTitle("");
          setPages(100);
        })
        .catch( (err: any) => {
          console.log( "There was a problem creating a new book.");
        });
      } else {
        element.classList.add('was-validated');
      }
  }

  return (
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={handleSubmit} noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn"
                  value={isbn} onChange={e => setIsbn( parseInt( e.target.value ) )} required />
                <div className="invalid-feedback">
                  An ISBN is required.
                </div>
              </div>

              <div className="col-md-8 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title"
                  value={title} onChange={e => setTitle( e.target.value )} required />
                <div className="invalid-feedback">
                  A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages"
                  value={pages} onChange={e => setPages( parseInt( e.target.value ) )} required />
                <div className="invalid-feedback">
                  The number of pages is required.
                </div>
              </div>
            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>

          {bookRows}

        </tbody>
      </table>

    </div>
  );
}

export default Books;
